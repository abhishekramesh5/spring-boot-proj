package io.zabhiproject.springbootstarter.course;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import io.zabhiproject.springbootstarter.topic.player;

@RestController
public class ClubController {
	
	@Autowired
	private ClubService clubService;
	

	@RequestMapping("/players/{id}/clubs")
	public List<Club> getAllClubs(@PathVariable String id) {
		return clubService.getAllClubs(id);
	}
	
	@RequestMapping("/players/{playerId}/clubs/{id}")
	public Club getClub(@PathVariable String id) {
		return clubService.getClub(id);
	}
	 
	 @RequestMapping(method=RequestMethod.POST,value="/players/{playerId}/clubs")
	 public void addClub(@RequestBody Club club,@PathVariable String playerId) {
		 club.setPlayer(new player(playerId, "", ""));
		 clubService.addClub(club);
	 }
	 
	 
	 @RequestMapping(method=RequestMethod.PUT,value="/players/{playerId}/clubs/{id}")
	 public void updateClub(@RequestBody Club club, @PathVariable String playerId,@PathVariable String id) {
		 club.setPlayer(new player(playerId, "", ""));
		 clubService.updateClub(club);
	 }
	  
	 @RequestMapping(method=RequestMethod.DELETE,value="/players/{playerId}/clubs/{id}")
	 public void deleteClub(@PathVariable String id) {
		 clubService.deleteClub(id);
	}
	 
}
