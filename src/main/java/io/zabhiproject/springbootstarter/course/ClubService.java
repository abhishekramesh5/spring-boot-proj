package io.zabhiproject.springbootstarter.course;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/* creates an instance of this service and keep it in memory*/

@Service
public class ClubService {
	 
	//getting instance to spring class is by autowiring
	@Autowired
	private ClubRepository clubRepository;
			 
	 public List<Club> getAllClubs(String playerId) {
		 //to fetch all data querying the service
		 List<Club> clubs = new ArrayList<>();
		 clubRepository.findByPlayerId(playerId).forEach(clubs::add);;
		 return clubs;
	 }

	 
	 public Club getClub(String id) {
		return clubRepository.findOne(id);
	 }


	public void addClub(Club topic) {
		// TODO Auto-generated method stub
		clubRepository.save(topic);
		
	}


	public void updateClub(Club course) {
		// TODO Auto-generated method stub
		clubRepository.save(course);
	}


	public void deleteClub(String id) {
		clubRepository.delete(id);

	}
	
}
