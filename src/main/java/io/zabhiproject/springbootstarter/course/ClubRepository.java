package io.zabhiproject.springbootstarter.course;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

public interface ClubRepository extends CrudRepository<Club,String>{

	//uses implementation of spring jpa 
	// getAllTopics()
	//getTopic(String id)
	//updateTopic(Topic t)
	//deleteTopic(String id)
	//contains logic for any entity class
	public List<Club> findByPlayerId(String playerId);
	
}
