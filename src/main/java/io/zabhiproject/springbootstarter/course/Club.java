package io.zabhiproject.springbootstarter.course;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class Club {
	
	/* in rdb we need a primary key*/
	@Id
	private String id;
	private String name;
	private String description;
	
	//add a link between course and topic, every course should have topic
	@ManyToOne
	private io.zabhiproject.springbootstarter.topic.player player;
	
	public io.zabhiproject.springbootstarter.topic.player getPlayer() {
		return player;
	}

	/* need to map objects of topic class to relational database table*/
	public Club() {

	}	
	
	public Club(String id, String name, String description, String playerId) {
		super();
		this.id = id;
		this.name = name;
		this.description = description;
		this.player = new io.zabhiproject.springbootstarter.topic.player(playerId,"","");
	}	
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}

	public void setPlayer(io.zabhiproject.springbootstarter.topic.player player) {
		this.player = player;
	}

	

}
