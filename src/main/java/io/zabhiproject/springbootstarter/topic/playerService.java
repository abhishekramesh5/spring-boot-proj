package io.zabhiproject.springbootstarter.topic;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/* creates an instance of this service and keep it in memory*/

@Service
public class playerService {
	 
	//getting instance to spring class is by autowiring
	@Autowired
	private PlayerRepository playerRepository;
			 
	 public List<player> getAllPlayers() {
		 //to fetch all data querying the service
		 List<player> players = new ArrayList<>();
		 playerRepository.findAll().forEach(players::add);
		 return players;
	 }

	 
	 public player getPlayer(String id) {
		return playerRepository.findOne(id);
	 }


	public void addPlayer(player player) {
		// TODO Auto-generated method stub
		playerRepository.save(player);
		
	}


	public void updatePlayer(String id,player player) {
		// TODO Auto-generated method stub
		playerRepository.save(player);
	}


	public void deletePlayer(String id) {
		playerRepository.delete(id);

	}
	
}
