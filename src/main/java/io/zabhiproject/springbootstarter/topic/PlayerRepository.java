package io.zabhiproject.springbootstarter.topic;

import org.springframework.data.repository.CrudRepository;

public interface PlayerRepository extends CrudRepository<player,String>{

	//uses implementation of spring jpa 
	// getAllTopics()
	//getTopic(String id)
	//updateTopic(Topic t)
	//deleteTopic(String id)
	//contains logic for any entity class
	
}
