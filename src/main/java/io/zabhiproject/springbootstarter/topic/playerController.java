package io.zabhiproject.springbootstarter.topic;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class playerController {
	
	@Autowired
	private playerService playerService;
	

	@RequestMapping("/players")
	public List<player> getAllTopics() {
		return playerService.getAllPlayers();
	}
	
	@RequestMapping("/players/{id}")
	public player getTopic(@PathVariable String id) {
		return playerService.getPlayer(id);
	}
	 
	 @RequestMapping(method=RequestMethod.POST,value="/players")
	 public void addTopic(@RequestBody player player) {
		 playerService.addPlayer(player);
	 }
	 
	 
	 @RequestMapping(method=RequestMethod.PUT,value="/players/{id}")
	 public void updateTopic(@RequestBody player player, @PathVariable String id) {
		 playerService.updatePlayer(id,player);
	 }
	  
	 @RequestMapping(method=RequestMethod.DELETE,value="/players/{id}")
	 public void deleteTopic(@PathVariable String id) {
		 playerService.deletePlayer(id);
	}
	 
}
