package io.zabhiproject;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ZabhiProjectDataApplication {

	public static void main(String[] args) {
		SpringApplication.run(ZabhiProjectDataApplication.class, args);
	}
}
