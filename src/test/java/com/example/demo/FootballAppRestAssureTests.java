package com.example.demo;

import static io.restassured.module.mockmvc.RestAssuredMockMvc.given;
import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.mockito.Matchers.isNull;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;


import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.zabhiproject.ZabhiProjectDataApplication;
import io.zabhiproject.springbootstarter.topic.playerController;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = ZabhiProjectDataApplication.class)
@AutoConfigureMockMvc
public class FootballAppRestAssureTests {
	
    @Autowired
    private playerController controller;	
    
    @BeforeClass
    public static void setup() {
        String port = System.getProperty("server.port");

        RestAssured.port = Integer.valueOf(8081);


        String basePath = System.getProperty("server.base");
        if(basePath==null){
            basePath = "/players";
        }
        RestAssured.basePath = basePath;

        String baseHost = System.getProperty("server.host");
        if(baseHost==null){
            baseHost = "http://localhost";
        }
        RestAssured.baseURI = baseHost;

    }   
	
	@Test
	public void givenUrl_validateResponse() {
		given()
    	.contentType(ContentType.JSON)		
		.standaloneSetup(controller).
		when().
			get("/players").
		then().
			body(containsString(""));
	}	

}
