package com.example.demo;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertThat;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;


import io.zabhiproject.ZabhiProjectDataApplication;
import io.zabhiproject.springbootstarter.topic.playerController;


@SpringBootTest(classes = ZabhiProjectDataApplication.class)
public class ApplicationTest {
	
	//We autowire to create instance of the controller
	@Autowired
	private playerController controller;
	
	//Sanity check, to check if context is creating your controller and you add an assertion
    @Test
    public void contextLoads() throws Exception {
    	assertThat(controller).isNull();
    }

}